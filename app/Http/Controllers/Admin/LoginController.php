<?php
  
namespace App\Http\Controllers\Admin;
  
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
  
    // use AuthenticatesUsers;
    
    // protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function admin(){
        return view('admin.login');
    }
    
    public function admin_login(Request $request)
    {   
        $input = $request->all();
  
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
  
        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'email';
        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'] , 'role' => 1)))
        {
            if (Auth::user()->role == 1) {
                return view('admin');
            }
        }else{
            return view('admin.login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
          
    }

    public function admin_page(){
        if(\Auth::check()){
            return view('admin');
        }else{
            return view('admin.login');
        }
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('admin/login');
    }

}