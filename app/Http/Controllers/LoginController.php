<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function user(){
        return view('login');
    }

    public function user_login(Request $request)
    {   
        $input = $request->all();
  
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
  
        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'email';
        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'], 'role' => array(2,3))))
        {
            if (Auth::user()->role == 2) {
                return view('professor');
            }
            if (Auth::user()->role == 3) {
                return view('student');
            }
        }else{
            return view('login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
          
    }

    public function user_page(){
        if(\Auth::check()){
            if (Auth::user()->role == 2) {
                return view('professor');
            }
            if (Auth::user()->role == 3) {
                return view('student');
            }
        }else{
            return view('login');
        }
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('/login');
    }
}
