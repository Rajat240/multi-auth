<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ProfessorController extends Controller
{
    public function ProfessorListing(){
   	  $professors = DB::table('users')->select('id','name','email','created_at')
      ->where('users.role' , '=' , 2 )
      ->get();
    
      return view('admin.professor_list',compact('professors'));
    }

    public function addNew(){

    	return view('admin.add_professor');

    }

    public function addNewProfessor(Request $request){

    	$request->validate([
    		'name' => 'required',
        'email' => 'required|confirmed',
        'password' => 'required|confirmed|min:6',
        'confirm_password' => 'min:6',
      ]);

    	$user = new User;

    	$user->name = Request('name');
    	$user->email = Request('email');
    	$user->confirm_email = Request('email_confirmation');
    	$user->password = Hash::make(Request('password'));
    	$user->confirm_password = Hash::make(Request('password_confirmation'));
    	$user->faculty_url = Request('url');
    	$user->faculty_phone = Request('phone');
    	$user->role = Request('role');
    	$user->save();

    	return redirect()->to('/professor');

    }

    public function update($id){

    	$professors = DB::table('users')
            ->select('id', 'name', 'email', 'confirm_email' , 'password'
            , 'confirm_password' , 'faculty_url' , 'faculty_phone', 'role')
            ->where('id' , '=' , [$id])->get();

            return view('admin.update_professor',compact('professors'));

    }

    public function update_professor(Request $request,$id ) {

      $request->validate([
        'name' => 'required',
        'email' => 'required|confirmed',
        'password' => 'required|confirmed|min:6',
        'confirm_password' => 'min:6',
      ]);
   	  
   	  $name = $request->input('name');
      $email = $request->input('email');
      $confirm_email = $request->input('email_confirmation');
      $password = Hash::make($request->input('password'));
      $confirm_password = $request->input('password_confirmation');
      $faculty_url = $request->input('url');
      $faculty_phone = $request->input('phone');
      $role = $request->input('role');
      DB::update('update users set name = ?,email = ?,confirm_email = ?,password = ?,confirm_password = ?,faculty_url = ?,faculty_phone = ?,role = ? where id = ?',[$name,$email,$confirm_email,$password,$confirm_password,$faculty_url,$faculty_phone,$role,$id]);
      //DB::update('update users set email = ? where id = ?',[$email,$id]);
      return redirect('/professor');
    }

    public function delete_professor($id)
	  {
    	DB::delete('delete from users where id = ?',[$id]);
    	return redirect()->back();
	  }
}
