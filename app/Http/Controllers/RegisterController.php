<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    public function register_student()
    {
        return view('student_register');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create_student(Request $request)
    {

    	$request->validate([
            'email' => 'required|confirmed',
            'password' => 'required|confirmed|min:6',
            'first_name' => 'required',
            'last_name' => 'required',
            'school' => 'required',
        ]);

    	$user = new User;

    	$user->email = Request('email');
    	$user->confirm_email = Request('email_confirmation');
    	$user->password = Hash::make(Request('password'));
    	$user->confirm_password = Hash::make(Request('password_confirmation'));
    	$user->name = Request('first_name'). ' ' .Request('last_name');
    	$user->first_name = Request('first_name');
    	$user->last_name = Request('last_name');
    	$user->school = Request('school');
    	$user->role = Request('role');
    	$user->save();

    	return redirect()->to('/login');
       
    }

    public function register_professor()
    {
        return view('professor_register');
    }

    protected function create_professor(Request $request)
    {

    	$request->validate([
    		'name' => 'required',
            'email' => 'required|confirmed',
            'password' => 'required|confirmed|min:6',
            'confirm_password' => 'min:6',
        ]);

    	$user = new User;

    	$user->name = Request('name');
    	$user->email = Request('email');
    	$user->confirm_email = Request('email_confirmation');
    	$user->password = Hash::make(Request('password'));
    	$user->confirm_password = Hash::make(Request('password_confirmation'));
    	$user->faculty_url = Request('url');
    	$user->faculty_phone = Request('phone');
    	$user->role = Request('role');
    	$user->save();

    	return redirect()->to('/login');
       
    }
}
