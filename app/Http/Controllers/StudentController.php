<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function StudentListing(){
   	  $students = DB::table('users')->select('id','name','first_name','last_name','email','created_at')
      ->where('users.role' , '=' , 3 )
      ->get();
    
      return view('admin.student_list',compact('students'));
    }

    public function addNew(){

    	return view('admin.add_student');

    }

    public function addNewStudent(Request $request){

    	$request->validate([
            'email' => 'required|confirmed',
            'password' => 'required|confirmed|min:6',
            'confirm_password' => 'min:6',
            'first_name' => 'required',
            'last_name' => 'required',
            'school'   =>  'required'
        ]);

    	$user = new User;

    	$user->email = Request('email');
    	$user->confirm_email = Request('email_confirmation');
    	$user->password = Hash::make(Request('password'));
    	$user->confirm_password = Hash::make(Request('password_confirmation'));
    	$user->first_name = Request('first_name');
    	$user->last_name = Request('last_name');
    	$user->name = Request('first_name')." ".Request('last_name');
    	$user->school = Request('school');
    	$user->role = Request('role');
    	$user->save();

    	return redirect()->to('/student');

    }

    public function update($id){

    	$students = DB::table('users')
            ->select('id', 'email', 'confirm_email' , 'password'
            , 'confirm_password', 'first_name', 'last_name', 'school', 'role')
            ->where('id' , '=' , [$id])->get();

            return view('admin.update_student',compact('students'));

    }

    public function update_student(Request $request,$id ) {

    	$request->validate([
            'email' => 'required|confirmed',
            'password' => 'required|confirmed|min:6',
            'confirm_password' => 'min:6',
            'first_name' => 'required',
            'last_name' => 'required',
            'school'   =>  'required'
        ]);
   	  
      	$email = $request->input('email');
      	$confirm_email = $request->input('email_confirmation');
      	$password = Hash::make($request->input('password'));
      	$confirm_password = Hash::make($request->input('password_confirmation'));
      	$first_name = $request->input('first_name');
      	$last_name = $request->input('last_name');
      	$school = $request->input('school');
      	$role = $request->input('role');
      	DB::update('update users set email = ?,confirm_email = ?,password = ?,confirm_password = ?,first_name = ?,last_name = ?,school = ?,role = ? where id = ?',[$email,$confirm_email,$password,$confirm_password,$first_name,$last_name,$school,$role,$id]);
      	//DB::update('update users set email = ? where id = ?',[$email,$id]);
      	return redirect('/student');
    }

    public function delete_student($id)
	  {
    	DB::delete('delete from users where id = ?',[$id]);
    	return redirect()->back();
	  }
}
