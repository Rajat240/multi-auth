<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class UniversityController extends Controller
{
    public function UniversityListing(){
   	  $university = DB::table('university')->select('id','name','location','created_at')
      ->get();
    
      return view('admin.university_list',compact('university'));
    }

    public function addNew(){

    	return view('admin.add_university');

    }

    public function addNewUniversity(Request $request){

    	$request->validate([
    		'name' => 'required',
            'email' => 'required|confirmed',
            'location' => 'required',
        ]);


    	$name = Request('name');
    	$email = Request('email');
    	$confirm_email = Request('email_confirmation');
    	$location = Request('location');

    	$values = array('name' => $name,'email' => $email,'confirm_email' => $confirm_email,'location' => $location);

    	DB::table('university')->insert($values);

    	return redirect()->to('/university');

    }

    public function update($id){

    	$university = DB::table('university')
            ->select('id', 'name', 'email', 'confirm_email' , 'location')
            ->where('id' , '=' , [$id])->get();

            return view('admin.update_university',compact('university'));

    }

    public function update_university(Request $request,$id ) {

    	$request->validate([
    		'name' => 'required',
            'email' => 'required|confirmed',
            'location' => 'required',
        ]);
   	  
   	    $name = $request->input('name');
      	$email = $request->input('email');
      	$confirm_email = $request->input('email_confirmation');
      	$location = $request->input('location');
      	
      	DB::update('update university set name = ?,email = ?,confirm_email = ?,location = ? where id = ?',[$name,$email,$confirm_email,$location,$id]);
      	//DB::update('update users set email = ? where id = ?',[$email,$id]);
      	return redirect('/university');
    }

    public function delete_university($id)
	  {
    	DB::delete('delete from university where id = ?',[$id]);
    	return redirect()->back();
	  }
}
