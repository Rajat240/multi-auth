<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            
            <a class="nav-link" href="{{ url('admin/dashboard') }}">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i>
                </div>
                    Dashboard
            </a>                                                      
            <a class="nav-link" href="{{ url('professor') }}">
                <div class="sb-nav-link-icon"><i class="fa fa-users" aria-hidden="true"></i>
                </div>
                    Manage Professors
            </a>
            <a class="nav-link" href="{{ url('student') }}">
                <div class="sb-nav-link-icon"><i class="fa fa-users" aria-hidden="true"></i>
                </div>
                    Manage Students 
            </a>
            <a class="nav-link" href="{{ url('university') }}">
                <div class="sb-nav-link-icon"><i class="fa fa-university" aria-hidden="true"></i>
                </div>
                    Manage Universities
            </a>
        </div>
    </div>                   
</nav>