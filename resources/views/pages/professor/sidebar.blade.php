<!-- sidebar code start -->


<div class="display-table-cell v-align box" id="navigation">
                <div class="navi">
                    <ul>
                        <li class="active"><a href="#"><i class="fa fa-user" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Professor</span></a></li>
                        <li><a href="#"><i class="fa fa-graduation-cap" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Student</span></a></li>
                        <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Rating</span></a></li>                        
                    </ul>
                </div>
</div>
<!-- sidebar code end -->