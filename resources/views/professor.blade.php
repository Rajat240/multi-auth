@include('pages.professor.header')
@include('pages.professor.sidebar')

<!-- main content section start-->
<div class="prof-main col-md-11 col-sm-11 col-10 mt-20 ml-auto">               
                <div class="prof-dashboard">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col- no-padding">
                            <div class="prof-left bckgd-light">
                                <h4>Find Professor at</h4>
                                 <h5 class="Uni-name">University Of Cambridge</h5>
                                    <form class="serch-bx d-md-inline-block">
                                        <div class="input-group">
                                            <input class="form-control" type="text" placeholder="Search for Professor..." aria-label="Search" aria-describedby="basic-addon2" />                                            
                                        </div>
                                    </form>
                                    <div class="src-results"> 
                                       <p>37 Professors Found</p>
                                        <select id="sortby">
                                          <option value="">Sort By</option>
                                          <option value="date">Date</option>
                                          <option value="rank">Ranking</option>
                                          <option value="latest">Latest</option>
                                        </select>
                                         <ul class="list-prof">
                                            <li>Professor 1 <span class="rating">4.2</span><br><span class="rvw">33 Reviews</span></li>
                                            <li>Professor 1 <span class="rating">4.2</span><br><span class="rvw">33 Reviews</span></li> 
                                            <li>Professor 1 <span class="rating">4.2</span><br><span class="rvw">33 Reviews</span></li>
                                            <li>Professor 1 <span class="rating">4.2</span><br><span class="rvw">33 Reviews</span></li> 
                                            <li>Professor 1 <span class="rating">4.2</span><br><span class="rvw">33 Reviews</span></li>                                             
                                         </ul>
                                    </div>
                                    
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col- ">
                            <div class="row bckgd-light">
                                 <div class="col-md-4 col-sm-4 col- ">
                                      <div class="first-block block-padding">
                                         <div class="iscon-cntr"><i class="fa fa-smile-o" aria-hidden="true"></i></div>
                                             <div class="rating-row">
                                                <div class="numbr-rati">5.0</div>
                                                <div class="rating-txt">Average<br> Rating</div>
                                             </div>
                                             <div class="rating-row">
                                                <div class="numbr-rati">4.2</div>
                                                <div class="rating-txt">Average<br> Difficulty</div>
                                             </div>
                                      </div>
                                 </div>
                                 <div class="col-md-4 col-sm-4 col- ">
                                    <div class="first-block block-padding">
                                        <h2>Trump,<br> Donald</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><br>
                                        <span>Lorem ipsum dolor sit?</span>
                                        <span>Lorem ipsum dolor sit amet</span>
                                    </div>
                                 </div>
                                 <div class="col-md-4 col-sm-4 col- ">
                                    <div class="first-block block-padding">
                                        <h4>Harvard University</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><br>
                                         <p>Lorem ipsum dolor sit amet, <strong>consectetur adipiscing elit</strong></p>
                                        <span>Lorem ipsum dolor sit?</span>
                                        <span>Lorem ipsum dolor sit amet</span>
                                    </div> 
                                 </div>
                            </div>
                            <!--student Reviews block-->
                        <div class="review-main">
                              <div class="row">
                                     <div class="col-md-8 col-sm-8 col- "><h3>Student Reviews</h3> </div>
                                     <div class="col-md-4 col-sm-4 col- "><a href="#" class="btn-dgn">Lorem Ipsum</a></div>
                             </div>
                            <div class="row bckgd-light student-rvew-sctn"> 
                                
                                 <div class="col-md-2 col-sm-2 col- ">
                                      <div class="first-block block-padding">                                        
                                             <div class="rating-row">                                               
                                                <div class="stu-rat-txt">Rating</div>
                                                <div class="stu-rati">5.0</div>
                                             </div>
                                             <div class="rating-row">
                                                <div class="stu-rat-txt">Difficulty</div>
                                                <div class="stu-rati">3.5</div>                                             
                                             </div>
                                      </div>
                                 </div>
                                 <div class="col-md-3 col-sm-3 col- ">
                                    <div class="first-block block-padding">
                                        <h5>Class Math2018</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p><br>
                                        
                                    </div>
                                 </div>
                                 <div class="col-md-7 col-sm-7 col- ">
                                    <div class="first-block block-padding">
                                        <h4>Harvard University</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        <div class="raiting-footer">
                                           <ul class="list-foot">
                                              <li>Lorem ipsum dolor<i class="fa fa-thumbs-up" aria-hidden="true"></i></li>
                                              <li>Lorem ipsum dolor sit<i class="fa fa-thumbs-down" aria-hidden="true"></i></li>
                                           </ul>
                                        </div>
                                    </div> 
                                 </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
 </div>
</body>





</html>     