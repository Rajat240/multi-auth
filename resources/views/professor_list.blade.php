<!DOCTYPE html>
<html lang="en">
   @include('pages.admin.header') 
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                @include('pages.admin.sidebar')  
            </div>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h2>Professors List</h2>
                    <div class="form-group">
                        <a class="btn btn-primary add-new" href="{{ url('professor/create')}}" ><i class="fa fa-plus"></i> Add New</a>
                    </div>
                        <table id="example" class="table table-striped table-bordered">
                            <div class="tbl">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Registered At</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($professors))

                                    @foreach($professors as $record)
                                    <tr>
                                        <td>{{ $record->id }}</td>
                                        <td>{{ $record->name }}</td>
                                        <td>{{ $record->email }}</td>
                                        <td>{{ $record->created_at }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info" href="professor/update/{{ $record->id }}" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a class="btn btn-sm btn-danger deleteListing" href="deleteProfessor/{{ $record->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach

                                    @endif
                                </tbody>
                            </div>
                        </table>
                    </div>
                </main> 
        </div>
</body>
</html>     