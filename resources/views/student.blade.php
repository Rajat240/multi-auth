@include('pages.student.header')
<!-- slider section start-->
<div class="hero">
     <div class="hero__background">
         <img src="{{ URL::asset('img/banner-student.jpg') }}" alt=""/>
     </div>
     <section class="hero__content">
         <h1>Find what you're looking for.</h1>
    </section>   
</div>

<!-- slider section end-->
<!-- three blocks section start-->
<div class="container">
    <div class="main-student">
        <div class="row">
            <div class="col-md-4 col- ">
                <div class="find-box">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <h3>Find the professor you're looking for.</h3>
                    <a class="fnd-btn" href="#">Find a Professor</a>
                </div>
            </div>
            <div class="col-md-4 col- ">
                <div class="find-box">
                    <div class="icon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                    <h3>Find the school you're looking for.</h3>
                    <a class="fnd-btn" href="#">Find a School</a>
                </div>
            </div>
            <div class="col-md-4 col- ">
                <div class="find-box">
                    <div class="icon"><i class="fa fa-star" aria-hidden="true"></i></div>
                    <h3>Find the outlet you're looking for.</h3>
                    <a class="fnd-btn" href="#">Rate a Professor</a>
                </div>
             </div>
         </div>
     </div>
</div>
<!-- three blocks section end-->


<!-- blog section start-->
<div class="container">
    <div class="news-box"> 
        <h2>Our Latest News & Articles</h2>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="news-post border-on layout-style1">
                    <img width="370" height="260" src="images/banner-student.jpg" alt=""/>
                        <div class="news-content">
                            <p class="auth-paragraph">
                            March 26, 2020 <a href="#">2 Comments</a></p>
                            <h2 class="heading2"><a href="#"></a></h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi...</p>
                            <a class="text-btn" href="#">Read More ...</a>
                         </div>
                </div>
            </div> 
            <!-- block code ends-->
            <div class="col-lg-4 col-md-6">
                <div class="news-post border-on layout-style1">
                    <img width="370" height="260" src="images/banner-student.jpg" alt=""/>
                        <div class="news-content">
                            <p class="auth-paragraph">
                            March 26, 2020 <a href="#">2 Comments</a></p>
                            <h2 class="heading2"><a href="#"></a></h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi...</p>
                            <a class="text-btn" href="#">Read More ...</a>
                         </div>
                </div>
            </div> 
            <!-- block code ends-->
            <div class="col-lg-4 col-md-6">
                <div class="news-post border-on layout-style1">
                    <img width="370" height="260" src="images/banner-student.jpg" alt=""/>
                        <div class="news-content">
                            <p class="auth-paragraph">
                            March 26, 2020 <a href="#">2 Comments</a></p>
                            <h2 class="heading2"><a href="#"></a></h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi...</p>
                            <a class="text-btn" href="#">Read More ...</a>
                         </div>
                </div>
            </div> 
        </div>
    </div>
</div>                  
</body>




</html>     