<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([ 'namespace' => 'Admin','prefix' => 'admin'], function () {
	Route::get('/login', 'LoginController@admin');
	Route::post('dashboard', 'LoginController@admin_login');
	Route::get('dashboard', 'LoginController@admin_page');
    Route::get('logout', 'LoginController@logout');
});

Route::get('/login', 'LoginController@user');
Route::post('dashboard', 'LoginController@user_login');
Route::get('dashboard', 'LoginController@user_page');
Route::get('logout', 'LoginController@logout');

Route::get('student/register', 'RegisterController@register_student');
Route::post('create/student', 'RegisterController@create_student');
Route::get('professor/register', 'RegisterController@register_professor');
Route::post('create/professor', 'RegisterController@create_professor');

Route::get('professor', 'ProfessorController@ProfessorListing');
Route::get('professor/create', 'ProfessorController@addNew');
Route::post('addNewProfessor', 'ProfessorController@addNewProfessor');
Route::get('professor/update/{id}', 'ProfessorController@update');
Route::post('professor_update/{id}', 'ProfessorController@update_professor');
Route::get('deleteProfessor/{id}', 'ProfessorController@delete_professor');

Route::get('student', 'StudentController@StudentListing');
Route::get('student/create', 'StudentController@addNew');
Route::post('addNewStudent', 'StudentController@addNewStudent');
Route::get('student/update/{id}', 'StudentController@update');
Route::post('student_update/{id}', 'StudentController@update_student');
Route::get('deleteStudent/{id}', 'StudentController@delete_student');

Route::get('university', 'UniversityController@UniversityListing');
Route::get('university/create', 'UniversityController@addNew');
Route::post('addNewUniversity', 'UniversityController@addNewUniversity');
Route::get('university/update/{id}', 'UniversityController@update');
Route::post('university_update/{id}', 'UniversityController@update_university');
Route::get('deleteUniversity/{id}', 'UniversityController@delete_university');





Route::get('/', function () {
    return view('home');
});




